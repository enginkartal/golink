<?php

require_once './go-core/class/urlShort.class.php';
require_once './go-core/class/app.class.php';

if (!empty($_GET['keyCode'])) {

    $url = new urlShort();
    $app = new app();

    $result = $url->urlInfo($_GET['keyCode']);


    if (empty($result)) {
        $app->redirect("index.php");
    } else {

        $redirectPage = $result["url"];

        $app->redirect($redirectPage);
    }
}
?>