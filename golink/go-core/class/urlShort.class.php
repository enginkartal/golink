<?php

class urlShort {

    private $urlTable = "gl_urlpools";
    private $pdo;
    private $exceptMessage = "";

    public function __construct() {
        try {
            $this->pdo = new PDO('mysql:host=localhost;dbname=dbname', "dbuser", "dbpass");
        } catch (PDOException $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }

    public function urlShortCode($url) {

        try {

            if (!$this->urlValidation($url)) {
                $this->exceptMessage = "Invalid URL";

                throw new Exception($this->exceptMessage);
            }

            $dbC = $this->DBControl($url);

            if (count($dbC) == 0) {

                $dbC = $this->insertDB($url);
            }

            echo json_encode($dbC[0]);
        } catch (Exception $ex) {
            $excAr['responseMessage'] = $ex->getMessage();
            $excAr['shortUrl'] = $ex->getMessage();
            
            echo json_encode($excAr);
        }
    }

    public function urlInfo($keyUrl) {
        
        $dbTable = $this->urlTable;

        $result = $this->pdo->prepare("select longUrl as url from $dbTable where keyUrl=:keyUrl");

        $result->bindParam(':keyUrl', $keyUrl);

        $result->execute();

        $rs = $result->fetch(PDO::FETCH_ASSOC);

        return $rs;
        
    }

    private function DBControl($url) {

        $dbTable = $this->urlTable;

        $result = $this->pdo->prepare("select longUrl as url,concat('http://demo.golink.in/',keyUrl) as shortUrl from $dbTable where longUrl=:url");

        $result->bindParam(':url', $url);

        $result->execute();

        $rs = $result->fetchAll(PDO::FETCH_ASSOC);

        return $rs;
    }

    private function insertDB($url) {
        try {

            $hash = $this->createShortCode($url);

            $dbTable = $this->urlTable;

            $rs = $this->pdo->exec("INSERT INTO $dbTable (longUrl,keyUrl) VALUES ('$url','$hash')");

            $nData = $this->DBControl($url);

            return $nData;
        } catch (Exception $ex) {

            $excAr['r'] = $ex->getMessage();

            echo json_encode($excAr);
        }
    }

    private function urlValidation($url) {

        $pattern = "#((http|https|ftp)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#ie";

        if (!preg_match($pattern, $url)) {
            return FALSE;
        }

        return TRUE;
    }

    private function createShortCode($url) {

        $ins = strtolower(base64_encode(md5(sha1(md5($url)))));

        $hash = "";

        $i = 0;
        while ($i < 4) {

            $hash.=$ins[$i];

            $i++;
        }

        return $hash;
    }

}

//## class
?>
