<?php

ob_start();
header("Content-Type:application/json");

require_once '../../../class/urlShort.class.php';

$headerData = array("status" => "400", "statusCode" => "Bad Request", "message" => "Invalid Value");

if (!empty($_GET['url'])) {

    $url = new urlShort();

    $data = $url->urlShortCode($_GET['url']);

    $headerData["status"] = "200";
    $headerData["statusCode"] = "OK";

    headerResponse($headerData["status"], $headerData["statusCode"], $data);
} else {

    $headerData["status"] = "400";
    $headerData["statusCode"] = "Bad Request";
    $headerData["message"] = "Invalid Value";
    
    headerResponse($headerData["status"], $headerData["statusCode"], json_encode($headerData));
}

function headerResponse($status, $statusCode, $data) {

    header("HTTP/1.1 " . $status . $statusCode . "");

    echo $data;
}
