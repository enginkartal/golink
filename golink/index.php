﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <!-- Bootstrap -->
            <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
                <link href="css/style.css" rel="stylesheet" media="screen">
                    <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
                    <script type="text/javascript" src="js/bootstrap.min.js"></script>
                    <script type="text/javascript">

                        $(function() {

                            function setResponse(value) {

                                $('#spnResult').text(value);

                            }


                            $('#go').on("click", function() {

                                var url = $('#url').val();
                                var data = {'url': url};

                                $.ajax({
                                    url: 'go-core/services/rest/1.0/urlshortener.php',
                                    data: data,
                                    contentType: 'application/json',
                                    dataType: 'json',
                                    success: function(r) {

                                        console.log(r);

                                        setResponse(r.shortUrl);
                                    },
                                    error: function(e, m, x) {

                                        setResponse(x);
                                    }
                                });

                            });

                        });

                    </script>
                    </head>
                    <body>

                        <h4>GoLink.in</h4>
                        <div id="Content">
                            <form class="bs-docs-example">
                                <div class="input-prepend">
                                    <input class="input-xxlarge" type="text" id="url" placeholder="Go URL">
                                        <button class="btn" id="go" type="button">Go Short!</button>
                                </div>
                                <p></p>
                                <span id="spnResult"></span>
                            </form>
                        </div>
                    </body>
                    </html>
